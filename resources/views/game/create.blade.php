@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Создание персонажа</div>

                <div class="card-body">
                    <form action="{{ route('game.store') }}" method="post">
                        @csrf
                        <label for="name">Имя персонажа</label>
                        <input type="text" name="name" id="name" required>
                        <input type="submit" value="Создать">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
