@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Список персонажей </div>

                <div class="card-body">
                    <a href="{{ route('game.create') }}">Создать персонажа</a>
                    <ul>
                        @foreach ($list as $item)
                        <li>
                            <form action="{{ route('game.select') }}" method="post">
                                @csrf
                                <input type="hidden" name="id" value="{{ $item->id }}">
                                <input type="submit" value="{{ $item->name }}">
                            </form>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
