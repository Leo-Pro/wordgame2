<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Character extends Model
{
    protected $fillable = [
        'id',
        'experience', //опыт
        'name', //имя
        'location_id', //локация
        'health',
        'updated_at',
        'created_at',
        'user_id',
     ];
    public function user() {
        return $this->belongsTo(User::class);
    }
}
