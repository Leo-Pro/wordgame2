<?php

namespace App\Http\Controllers;

use App\Tools\CharacterCharacteristics;
use App\Models\Character;
use App\Http\Requests\CharacterCreateRequest;
use Illuminate\Http\Request;

class CharacterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('game.index', [
                'list' => Character::get(),
                ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('game.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CharacterCreateRequest $request)
    {
        $character = new Character();
        $character->user_id = $request->user()->id;
        $character->name = $request->input('name');
        $character->experience = 0;
        $character->health = 100;
        $character->location_id = 1;
        $character->save();
		dd(Character::get(id));
        return redirect()->route('game.select');
    }

     /**
     *
     *
     * @param  int  Character $character
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function select(Request $request)
    {
        $id = $request->input('id');
        $request->session()->put(CharacterCharacteristics::MY_CHARACTER_SESSION_KEY, $id);
        return redirect()->route('game.show');
    }
    /**

     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Reques t  $request
     * @return \Illuminate\Http\Response
     * @return  \app\Tools\CharacterCharacteristics
     */
    public function show(Request $request)
    {
         $id =  $request->session()->get(CharacterCharacteristics::MY_CHARACTER_SESSION_KEY);
         return view('game.show',  [
                'character'  => Character::find($id),
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
