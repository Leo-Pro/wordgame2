<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\Character;
use App\Tools\CharacterCharacteristics;

class HasSelectedCharacter
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $redirectRoute = redirect()->route('game.index');
        $characterId = $request->session()->get(CharacterCharacteristics::MY_CHARACTER_SESSION_KEY, 0);
        $character = Character::find($characterId);

        if (!$characterId) {
            return $redirectRoute;
        }

        if (!$character) {
            return $redirectRoute;
        }

        return $next($request);
    }
}
