<?php

use App\Http\Controllers\CharacterController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/game', [CharacterController::class, 'index'])->name('game.index');
Route::get('/game/create', [CharacterController::class, 'create'])->name('game.create');
Route::post('/game/create', [CharacterController::class, 'store'])->name('game.store');
Route::post('/game/select', [CharacterController::class, 'select'])->name('game.select');
/*
Route::middleware('HasSelectedCharacter')->prefix('game')->group(function () {
    Route::get('show', [CharacterController::class, 'show'])->name('game.show');
});
*/
Route::group(['prefix' => 'game', 'middleware' => ['HasSelectedCharacter']], function () {
    Route::get('show', [CharacterController::class, 'show'])->name('game.show');
});
